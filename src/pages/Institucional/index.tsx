import React from "react";

import { CardSelect } from "../../components/CardSelect/indexcardSelect";


import "./styles.css";

const Institutional = () => {

    return (
    <div className="wrapper">
        
        
        <div className="institucional-container">
        
            <div className="container-titulo-institucional">
            <h1 className="institucional-titulo">Institucional</h1>
            </div>
            <CardSelect/>
        </div>
        

    </div>
    )
}




export { Institutional };