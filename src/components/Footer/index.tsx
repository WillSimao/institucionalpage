import React from "react";

import { NewsletterForm } from "./NewsLetter/index";
import { Information } from "./CardInformaçõesFooter/index";
import { Copyright } from "./FooterCopyright/index";


import "./styles.css";


const Footer = () => {



  return (
    <footer className="footer-container">
        <NewsletterForm/>
        <Information/>
        <Copyright/>
    </footer>
  )};


export { Footer };
