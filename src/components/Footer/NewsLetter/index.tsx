import React from "react";

import { Formik, Form, Field, ErrorMessage } from "formik";

import "./styles.css";

import NewsletterSchema from "../../../schema/NewsletterSchema";
import { ScrollTop } from "../../ScrollTop/ScrollTop";
import Whatsapp from "../../../assets/ImagesFooter/whatsapp.svg";



interface IFormikValues {
    email: string;
  }
  
  const initialValues = {
    email: "",
  };
  




const NewsletterForm = () => {
    const SubmitNewsletter = (values: IFormikValues) => {
        console.log(values);
}

return (
<div className="container-newsletter">
        <Formik
          onSubmit={SubmitNewsletter}
          initialValues={initialValues}
          validationSchema={NewsletterSchema}
        >
          {({ errors, touched }) => (

            <Form className="container-input">
              <label htmlFor="newsletter">Assine nossa Newsletter</label>
              <Field
                id="newsletter"
                name="newsletter"
                placeholder="E-mail"
                className={errors.email && touched.email && "invalid"}
              />
              <ErrorMessage
                component="span"
                name="newsletter"
                className="newsletter-component-span"
              />
              <button className="newsletter-input-button" type="submit">
                Enviar
              </button>
            </Form>
          )}
        </Formik>

        <div className="contact-wrapper">
          <div className="contact-icons">

            <div className="flex align-end">
              <a href="https://www.whatsapp.com/?lang=pt_br"><img className="img-whatsapp" src={Whatsapp} alt="Whatsapp" /></a>
            </div>

            <div className="flex align-end">
                <ScrollTop/>
            </div>

          </div>
        </div>
      </div>

          )}


export { NewsletterForm };