import React from "react"

import "./styles.css";

import Facebook from "../../../assets/ImagesFooter/facebook.svg";
import Instagram from "../../../assets/ImagesFooter/instagram.svg";
import Twitter from "../../../assets/ImagesFooter/twitter.svg";
import Youtube from "../../../assets/ImagesFooter/youtube.svg";
import Linkedin from "../../../assets/ImagesFooter/linkedin.svg";


const Information = () => {

    return (
        <div className="wrapper-informacoes">
        <div className="container-informacoes">
          <div className="wrapper-link-pages" >

            <div className="link-page-institucional">
            <h1 className="page-institucional-title" >Institucional</h1>
              <p>Quem Somos</p>
              <p>Política de Privacidade</p>
              <p>Segurança</p>
              <p>Seja um Revendedor</p>  
            </div>

          <div>
            <h1>Dúvidas</h1>
              <p>Entrega</p>
              <p>Pagamento</p>
              <p>Trocas e Devoluções</p>
              <p>Dúvidas Frequentes</p>
          </div>
          
          <div>
            <h1>Fale Conosco</h1>
              <h2>Atendimento ao Consumdor</h2>
              <p>(11) 4159-9504 </p>
              <h2>Atendimento Online</h2>
              <p>(11) 99433-8825</p>
          </div>
          </div>


      
        
              
          <div className="wrapper-midias" >
              <img src={Facebook} alt="Facebook" />
              <img className="img-instagram" src={Instagram} alt="Instagram" />
              <img src={Twitter} alt="Twitter" />
              <img className="img-youtube" src={Youtube} alt="Youtube" />
              <img src={Linkedin} alt="Linkedin" />
              <p>www.loremimpsum.com</p>
          </div>
          

      </div>
      </div>
    )
}

export { Information };