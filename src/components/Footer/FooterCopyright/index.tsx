import React from "react";

import "./styles.css";

import Master from "../../../assets/ImagesFooter/mastercard.svg";
import Visa from "../../../assets/ImagesFooter/visa.svg";
import AmericaEx from "../../../assets/ImagesFooter/americaxpress.svg";
import Elo from "../../../assets/ImagesFooter/elo.svg";
import Hipecard from "../../../assets/ImagesFooter/hipercard.svg";
import Paypal from "../../../assets/ImagesFooter/paypal.svg";
import Boleto from "../../../assets/ImagesFooter/boleto.svg";
import Vtex from "../../../assets/ImagesFooter/vtex.svg";
import VtexM3 from "../../../assets/ImagesFooter/vtex-logo-m3.svg";


const Copyright =() => {
    return (

        <div className="container-copyright">
            
            <div className="wrapper-copyright-paragrafo">
                <p className="copyright-paragrafo" >Lorem ipsum dolor sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor</p>
            </div>

            <div className="wrapper-cards" >
                <div className="border" >
                <img src={Master} alt="Cartão Master"  />
                <img src={Visa} alt="Cartão Visa"/>
                <img src={AmericaEx} alt="Cartão America Express"/>
                <img src={Elo} alt="Cartão Elo"/>
                <img src={Hipecard} alt="Cartão Hipecard"/>
                <img src={Paypal} alt="Cartão Paypal"/>
                <img className="wrapper-cards-boleto" src={Boleto} alt="Boleto"/>
                </div>
                <a href="https://vtex.com/br-pt/overview-plataforma/?utm_source=google&utm_medium=cpc&utm_campaign=BR_VTEX_Search_Branded&utm_term=vtex&utm_content=vtex_523171640535"><img src={Vtex} alt="Plataforma Vtex.io"/></a>
            </div>

            <div className="wrapper-logo-m3vtex" >
                <img src={VtexM3} alt="LogoM3Vtex" />
            </div>

        </div>

    )
};


export { Copyright };