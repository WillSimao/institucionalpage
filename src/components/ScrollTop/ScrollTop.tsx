import React, { useEffect, useState } from "react";

import "./ScrollTop.css"

import Seta from "../../assets/ImagesFooter/setaEllipse.svg";


const ScrollTop = () => {
  const [isVisable, setIsVisible] = useState(false);

  const toogleVisibility = () => {
    if (window.pageYOffset > 100) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toogleVisibility);

    return () => {
      window.removeEventListener("scroll", toogleVisibility);
    };
  }, []);

  return (
    <div
      className={
        isVisable ? "scrolltop-container" : "scrolltop-container-off"
      }
    >
      <button
        className="scrolltop-button-container"
        type="button"
        onClick={scrollTop}
      >
        <img
          src= {Seta}
          alt="Seta indicando scroll para o topo"
        />
      </button>
    </div>
  );
}


export { ScrollTop }