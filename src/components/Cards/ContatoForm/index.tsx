import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import FormSchema from "../../../schema/FormSchema";

import "./styles.css";

interface IFormikValues {
    name: string;
    email: string;
    cpf: string;
    birthDate: string;  
    phoneNumber: string;
    instagram: string;
    acceptTerms: boolean;
};

const initialValues = {
    name: "",
    email: "",
    cpf: "",
    birthDate: "",
    phoneNumber: "",
    instagram: "",
    acceptTerms: false,
};

const FormFormik = () => {
    const handleSubmitForm = (values: IFormikValues) => {
        console.log(values);
    };

    return (

        <div className="form-wrapper">

            <Formik onSubmit={handleSubmitForm} 
            initialValues={initialValues}
            validationSchema={FormSchema}
            >
                {({errors, touched}) => (
                     
                        <Form>
        
                        <h1 className="form-titulo">preencha o formulário</h1>
                        
                        <div className="form-input" >
                        <label htmlFor="name">Nome</label>
                         <Field id="name" name="name" placeholder="Seu nome completo" className={errors.name && touched.name && "invalid"} />
                          <ErrorMessage 
                          component="span" 
                          name="name" 
                          className="form-invalid-feedback"/>
                        </div>
                     
                        <div className="form-input" >
                        <label htmlFor="email">E-mail</label>
                         <Field id="email" name="email" placeholder="Seu e-mail" className={errors.email && touched.email && "invalid"} />
                         <ErrorMessage 
                         component="span" 
                         name="email" 
                         className="form-invalid-feedback" /> 
                        </div>
                    
                     
                        <div className="form-input" >
                        <label htmlFor="cpf">CPF</label>
                         <Field id="cpf" name="cpf" placeholder="000 000 000 00" className={errors.cpf && touched.cpf && "invalid"} />
                         <ErrorMessage 
                         component="span" 
                         name="cpf" 
                         className="form-invalid-feedback" />
                         </div> 
                     
                     
                         <div className="form-input" >
                        <label htmlFor="birthDate">Data de Nascimento:</label>
                         <Field id="birthDate" name="birthDate" placeholder="00 . 00 . 0000" className={errors.birthDate && touched.birthDate && "invalid"} />
                         <ErrorMessage 
                         component="span" 
                         name="birthDate" 
                         className="form-invalid-feedback" />
                         </div> 
                     
                     
                        <div className="form-input" >
                        <label htmlFor="phoneNumber">Telefone:</label>
                         <Field id="phoneNumber" name="phoneNumber" placeholder="(+00) 00000 0000" className={errors.phoneNumber && touched.phoneNumber && "invalid"} />
                         <ErrorMessage 
                         component="span" 
                         name="phoneNumber"
                         className="form-invalid-feedback" />
                        </div>
                     
                     
                        <div className="form-input" >
                        <label htmlFor="instagram">Instagram</label>
                         <Field id="instagram" name="instagram" placeholder="@seuuser" className={errors.instagram && touched.instagram && "invalid"}  />
                         <ErrorMessage 
                         component="span" 
                         name="instagram" 
                         className="form-invalid-feedback" />
                         </div>
                         

                         <div className="form-checkbox-container">
                            <label htmlFor="acceptTerms">
                            <span className="form-checkbox-alert">*</span>
                            <u>Declaro que li e aceito</u>
                            </label>
                            <Field
                            id="acceptTerms"
                            name="acceptTerms"
                            type="checkbox"
                            className={
                            errors.acceptTerms && touched.acceptTerms && "invalid"
                            }
                            />
                            <ErrorMessage
                            component="span"    
                            name="acceptTerms"
                            className="form-invalid-feedback"
                            />
                            </div>

                        <div className="form-input-button" >
                        <button className="form-button" type="submit">cadastre-se</button>
                        </div>
                     
                 </Form>
                )}
            </Formik>
         </div>
    );  
};



export { FormFormik };