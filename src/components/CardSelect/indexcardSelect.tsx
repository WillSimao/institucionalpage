   import React, { useState } from "react";

import "./cardSelect.css";
import "./buttonRender.css";

import { FormFormik } from "../Cards/ContatoForm/index";
import { FormaDePagamento } from "../Cards/FormaDePagamento/index";
import { Entrega } from "../Cards/Entrega/index";
import { TrocaEDevolucao } from "../Cards/TrocaEDevolução/index";
import { SegurancaEPrivacidade } from "../Cards/SegurançaEPrivacidade/index";
import { SobreLoja } from "../Cards/Sobre/index";

const CardSelect = () => {
   const [render, setRender] = useState ("FirstCard");

   return (
      
         <div className="card-select-container" >
            <div className="btn-card-container" >
               <button onClick={()=> setRender("FirstCard")} className="btn-card-sobre">Sobre</button>
               <button onClick={()=> setRender("SecondCard")} className="btn-card-forma-de-pagamento">Forma de Pagamento</button>
               <button onClick={()=> setRender("ThirdCard")} className="btn-card-entrega">Entrega</button>
               <button onClick={()=> setRender("FourthCard")} className="btn-card-troca-e-devolucao">Troca e Devolução</button>
               <button onClick={()=> setRender("FifthCard")} className="btn-card-seguranca-e-privacidade">Segurança e Privacidade</button>
               <button onClick={()=> setRender("SixthCard")} className="btn-card-contato">Contato</button>
            </div>

            <div>
               {render === "FirstCard" && <SobreLoja/>}
               {render === "SecondCard" && <FormaDePagamento/>}
               {render === "ThirdCard" && <Entrega/>}
               {render === "FourthCard" && <TrocaEDevolucao/>}
               {render === "FifthCard" && <SegurancaEPrivacidade/>}
               {render === "SixthCard" && <FormFormik/>}
            </div>
         </div>
      
   )


}

export { CardSelect };