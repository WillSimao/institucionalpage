import React from "react";

import Logo from "../../assets/ImagesGlobal/logom3academmy.svg";
import CarrinhoDeCompras from "../../assets/ImagesHeader/carrinhoCompras.svg";

import "./HeaderPage.css";

const HeaderPage = () => {
  return (
    <header className="page-header">
      <div className=" page-header-container">
        <a href="./"><img className="header-logo" src={Logo} alt="Logo m3Academmy" /></a>
        <div className="page-heade-container-input">
          <div className="input-content">
            <input
              className="header-input"
              type="text"
              name="buscar"
              id="buscar"
              placeholder="Buscar..."
            />
            <button type="submit" className="header-input-img">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  viewBox="0 0 18 18"
                  fill="currentColor"
                >
                  <path
                    d="M13.2094 11.6187C14.0951 10.4091 14.6249 8.92334 14.6249 7.31267C14.6249 3.2807 11.3444 0.000183105 7.31245 0.000183105C3.28048 0.000183105 0 3.2807 0 7.31267C0 11.3446 3.28052 14.6252 7.31248 14.6252C8.92315 14.6252 10.409 14.0953 11.6186 13.2095L16.4092 18.0001L18 16.4093C18 16.4092 13.2094 11.6187 13.2094 11.6187ZM7.31248 12.3751C4.52086 12.3751 2.25001 10.1043 2.25001 7.31267C2.25001 4.52104 4.52086 2.25019 7.31248 2.25019C10.1041 2.25019 12.375 4.52104 12.375 7.31267C12.375 10.1043 10.1041 12.3751 7.31248 12.3751Z"
                    fill="#292929"
                  />
                </svg>
            </button>
          </div>
        </div>
        <div className="header-container-usuario">
          <a href="/" className="header-loguin-loguin">Entrar</a>
          <img
            className="header-img-carrinho"
            src={CarrinhoDeCompras}
            alt="Carrinho de Compras"
          />
        </div>
      </div>
    </header>
  );
};

export { HeaderPage };
