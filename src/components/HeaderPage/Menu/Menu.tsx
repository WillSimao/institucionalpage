import React from "react";
import "./Menu.css";

const Menu: React.FC = () => {
  return (
    <div className="header-menu-container">
      <div className="header-menu-nav">
        <div className="header-menu-content">
          <a href="/"className="header-menu-nav-cursos">cursos</a>
          <a href="/"className="header-menu-nav-saibamais">saiba mais</a>
        </div>
      </div>
    </div>
  );
};

export { Menu };
