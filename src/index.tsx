import React from "react";
import ReactDOM from "react-dom/client";
import "./Main.css";

import { HeaderPage } from "./components/HeaderPage/HeaderPage";
import { Menu } from "./components/HeaderPage/Menu/Menu";
import { Institutional } from "./pages/Institucional/index";
import { Footer } from "./components/Footer/index";

function App() {
  return (
    <>
      <HeaderPage />
      <Menu />
      <Institutional />
      <Footer/>
    </>
  );
}

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(<App />);
